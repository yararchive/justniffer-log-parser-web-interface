var app = {
  // По какому адресу стучаться к API
  API_host: "http://localhost:3000",
  // Показывает индикатор загрузки
  showLoadIndicator: function() {
    $("#spinningSquaresG-conatiner").show();
  },
  // Скрывает индикатор загрузки
  hideLoadIndicator: function() {
    $("#spinningSquaresG-conatiner").hide();
  },
  // Скрывает стартовый экран
  mainLoadComplete: function() {
    $(".main-loader").hide();
  },
  // Подготовка таблицы с результатми
  prepareLogContentTable: function() {
    $("#log-content").empty();
    $("#log-content-table").show();
  },
  // Инициализация приложения
  init: function(){
    new app.Routers.Main();
  },

  Routers: {
    Main: Backbone.Router.extend({
      routes: {
        "": "emptyRoute",
        "*other": "wrongRoute"
      },

      initialize: function() {
        Backbone.history.start();
        console.log("Router initialized");
      },

      emptyRoute: function() {
        // Инициализируем app.Views.Main
        new app.Views.Main();
      },

      wrongRoute: function() {
        console.log("You have selected wrong route");
      }
    })
  },

  Models: {
    justnifferLogRecord: Backbone.Model.extend({
      initialize: function() {
        // console.log("justnifferLogRecord model initialized");
      }
    })
  },

  Collections: {
    justnifferLogRecords: Backbone.Collection.extend({
      initialize: function() {
        this.model = app.Models.justnifferLogRecord;
        this.url = app.API_host;
        console.log("justnifferLogRecords collection initialized");
      }
    })
  },

  Views: {
    JustnifferLogRecord: Backbone.View.extend({
      tagName: "tr",

      initialize: function() {
        this.template = _.template($("#report-row").html());
      },

      render: function() {
        template = _.template(this.$el.html());
        this.$el.html(this.template(this.model.toJSON()));
        return this;
      }
    }),

    Main: Backbone.View.extend({
      el: $("#justniffer-log-parser-container"),

      events: {
        "click #simple-search-begin":  "startSimpleSearch"
      },

      initialize: function() {
        console.log("Main view initialized");
      },

      startSimpleSearch: function() {
        console.log("Simple search started");
        app.showLoadIndicator(); app.prepareLogContentTable();
        // Если в форме не забит временной промежуток, значит, возьмем сегодняшний день
        var now = new Date();
        var period_from = $("#period-from").val() ? $("#period-from").val() : now.getFullYear() + "-" + now.getMonth() + "-" + now.getDay();
        var period_to = $("#period-to").val() ? $("#period-to").val() : now.getFullYear() + "-" + now.getMonth() + "-" + now.getDay();
        // Из-за специфики кроссдоменных запросов приходится получать данные через задницу
        $.ajax({
          method: "get",
          dataType: "jsonp",
          url: app.API_host,
          data: {
            "period_from": period_from,
            "period_to": period_to,
            "source_ip": $("#userIP").val()
          },
          success: function(response) {
            records = new app.Collections.justnifferLogRecords(response);
            trafficTotal = 0;
            records.each(function(record) {
              row_view = new app.Views.JustnifferLogRecord({
                model: record
              });
              $("#log-content").append(row_view.render().el);
              trafficTotal += Number(record.get("response_size"));
            });
            // Покажем, сколько пользовател(и) израсходовали трафика за выбранный промежуток времени
            $("#traffic-summary").text(trafficTotal + " байт");
            app.hideLoadIndicator();
          },
          error: function() {
            console.log("API server error");
          }
        });
      }
    })
  }
}

$(document).ready(function() {
  // Скрыть индикаторы загрузки
  app.mainLoadComplete(); app.hideLoadIndicator();
  // Скроем таблицу с результатми поиска, она пока не нужна
  $("#log-content-table").hide();
  // Сгенерировать календари для выбора диапазона дат
  $('#period-from, #period-to').daterangepicker({
    presetRanges: [
      {text: 'Вчера', dateStart: 'yesterday', dateEnd: 'yesterday' },
      {text: 'Сегодня', dateStart: 'today', dateEnd: 'today' },
      {text: 'Последние 7 дней', dateStart: 'today-7days', dateEnd: 'today' },
      {text: 'Последние 30 дней', dateStart: 'Today-30', dateEnd: 'Today' }
    ],
    rangeStartTitle: "Начальная дата",
    rangeEndTitle: "Конечная дата",
    presets: {
      specificDate: "Определенный день", 
      dateRange: "Временной отрезок"
    },
    nextLinkText: 'Вперед',
    prevLinkText: 'Назад',
    doneButtonText: 'Готово',
    dateFormat: 'yy-mm-dd' // date formatting. Available formats: http://docs.jquery.com/UI/Datepicker/%24.datepicker.formatDate
  });
  // Меню в левой части
  $( ".accordion" ).accordion({
    heightStyle: "content"
  });

  app.init();
});